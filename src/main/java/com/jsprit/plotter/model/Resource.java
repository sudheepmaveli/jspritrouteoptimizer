package com.jsprit.plotter.model;

import java.util.List;

public class Resource {
	public String __type;
    public List<DeliveryLocation> destinations;
    public List<DeliveryLocation> origins;
    public List<Result> results;
	public String get__type() {
		return __type;
	}
	public void set__type(String __type) {
		this.__type = __type;
	}
	public List<DeliveryLocation> getDestinations() {
		return destinations;
	}
	public void setDestinations(List<DeliveryLocation> destinations) {
		this.destinations = destinations;
	}
	public List<DeliveryLocation> getOrigins() {
		return origins;
	}
	public void setOrigins(List<DeliveryLocation> origins) {
		this.origins = origins;
	}
	public List<Result> getResults() {
		return results;
	}
	public void setResults(List<Result> results) {
		this.results = results;
	}
    
}
