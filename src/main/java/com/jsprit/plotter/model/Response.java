package com.jsprit.plotter.model;

import java.util.List;

public class Response {
	
	private List<String> order;
	private List<DeliveryLocation> data;

	public List<String> getOrder() {
		return order;
	}

	public void setOrder(List<String> order) {
		this.order = order;
	}

	public List<DeliveryLocation> getData() {
		return data;
	}

	public void setData(List<DeliveryLocation> data) {
		this.data = data;
	}

}
