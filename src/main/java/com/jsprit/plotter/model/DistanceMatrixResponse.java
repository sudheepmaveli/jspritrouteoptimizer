package com.jsprit.plotter.model;

import java.util.List;

public class DistanceMatrixResponse {
	public String authenticationResultCode;
	public String brandLogoUri;
	public String copyright;
	public List<ResourceSet> resourceSets;
	public int statusCode;
	public String statusDescription;
	public String traceId;
	public String getAuthenticationResultCode() {
		return authenticationResultCode;
	}
	public void setAuthenticationResultCode(String authenticationResultCode) {
		this.authenticationResultCode = authenticationResultCode;
	}
	public String getBrandLogoUri() {
		return brandLogoUri;
	}
	public void setBrandLogoUri(String brandLogoUri) {
		this.brandLogoUri = brandLogoUri;
	}
	public String getCopyright() {
		return copyright;
	}
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	public List<ResourceSet> getResourceSets() {
		return resourceSets;
	}
	public void setResourceSets(List<ResourceSet> resourceSets) {
		this.resourceSets = resourceSets;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	@Override
	public String toString() {
		return "DistanceMatrixResponse [authenticationResultCode=" + authenticationResultCode + ", brandLogoUri="
				+ brandLogoUri + ", copyright=" + copyright + ", resourceSets=" + resourceSets + ", statusCode="
				+ statusCode + ", statusDescription=" + statusDescription + ", traceId=" + traceId
				+ ", getAuthenticationResultCode()=" + getAuthenticationResultCode() + ", getBrandLogoUri()="
				+ getBrandLogoUri() + ", getCopyright()=" + getCopyright() + ", getResourceSets()=" + getResourceSets()
				+ ", getStatusCode()=" + getStatusCode() + ", getStatusDescription()=" + getStatusDescription()
				+ ", getTraceId()=" + getTraceId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
}
