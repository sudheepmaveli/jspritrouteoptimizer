package com.jsprit.plotter.model;

import java.util.List;

public class ArrangeLocation {
	List<DeliveryLocation> locations;

	public List<DeliveryLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<DeliveryLocation> locations) {
		this.locations = locations;
	}
	

}
