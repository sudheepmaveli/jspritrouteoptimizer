package com.jsprit.plotter.model;

import java.util.List;

public class DistanceMatrixRequest {

	private List<DeliveryLocation> origins;
	private List<DeliveryLocation> destinations;
	private String travelMode;
	public List<DeliveryLocation> getOrigins() {
		return origins;
	}
	public void setOrigins(List<DeliveryLocation> origins) {
		this.origins = origins;
	}
	public List<DeliveryLocation> getDestinations() {
		return destinations;
	}
	public void setDestinations(List<DeliveryLocation> destinations) {
		this.destinations = destinations;
	}
	public String getTravelMode() {
		return travelMode;
	}
	public void setTravelMode(String travelMode) {
		this.travelMode = travelMode;
	}

	
}
