package com.jsprit.plotter.model;

public class Result {
	public int destinationIndex;
	public int originIndex;
	public int totalWalkDuration;
	public double travelDistance;
	public double travelDuration;
	public int getDestinationIndex() {
		return destinationIndex;
	}
	public void setDestinationIndex(int destinationIndex) {
		this.destinationIndex = destinationIndex;
	}
	public int getOriginIndex() {
		return originIndex;
	}
	public void setOriginIndex(int originIndex) {
		this.originIndex = originIndex;
	}
	public int getTotalWalkDuration() {
		return totalWalkDuration;
	}
	public void setTotalWalkDuration(int totalWalkDuration) {
		this.totalWalkDuration = totalWalkDuration;
	}
	public double getTravelDistance() {
		return travelDistance;
	}
	public void setTravelDistance(double travelDistance) {
		this.travelDistance = travelDistance;
	}
	public double getTravelDuration() {
		return travelDuration;
	}
	public void setTravelDuration(double travelDuration) {
		this.travelDuration = travelDuration;
	}
	@Override
	public String toString() {
		return "Result [destinationIndex=" + destinationIndex + ", originIndex=" + originIndex + ", totalWalkDuration="
				+ totalWalkDuration + ", travelDistance=" + travelDistance + ", travelDuration=" + travelDuration + "]";
	}
	
	
}

