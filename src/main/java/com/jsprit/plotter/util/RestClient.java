package com.jsprit.plotter.util;

import org.slf4j.LoggerFactory;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.apache.http.HttpHost;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.client.HttpClient;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import javax.net.ssl.HostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import java.security.KeyStore;
import org.apache.http.ssl.SSLContexts;
import java.net.URI;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.util.MultiValueMap;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpHeaders;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Service;

@Service
public class RestClient
{
    @Autowired
    RestTemplate restTemplate;
    private String proxyIp;
    private Integer proxyPort;
    private boolean isProxyNeeded;
    private String protocol;
    private static final Logger LOGGER;
    
    public RestClient() {
        this.isProxyNeeded = false;
    }
   
    
    public String getData(final String url) {
        RestClient.LOGGER.info("RestClient : postData : url :" + url);
        String respsonseData = null;
        try {
            final SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial((KeyStore)null, (chain, authType) -> true).build();
            final SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, (String[])null, (HostnameVerifier)new NoopHostnameVerifier());
            HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory;
            if (this.isProxyNeeded) {
                HttpClientBuilder setSSLSocketFactory = null;
                HttpHost proxy = null;
                httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory((HttpClient)setSSLSocketFactory.setProxy(proxy).build());
                setSSLSocketFactory = HttpClientBuilder.create().setSSLSocketFactory((LayeredConnectionSocketFactory)sslsf);
                proxy = new HttpHost(this.proxyIp, (int)this.proxyPort, this.protocol);
            }
            else {
                httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory((HttpClient)HttpClientBuilder.create().setSSLSocketFactory((LayeredConnectionSocketFactory)sslsf).build());
            }
            final HttpComponentsClientHttpRequestFactory requestFactory = httpComponentsClientHttpRequestFactory;
            final URI targetUrl = new URIBuilder(url).build();
            final RestTemplate restTemplate = new RestTemplate((ClientHttpRequestFactory)requestFactory);
            requestFactory.setReadTimeout(120000);
            requestFactory.setConnectTimeout(120000);
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            final HttpEntity<String> entity = (HttpEntity<String>)new HttpEntity((MultiValueMap)headers);
            respsonseData = (String)restTemplate.getForObject(targetUrl, (Class)String.class);
            RestClient.LOGGER.info("RestClient : postData : respsonseData: Success");
            RestClient.LOGGER.debug("RestClient : postData : respsonseData : " + respsonseData);
        }
        catch (HttpClientErrorException e) {
            RestClient.LOGGER.error("RestClient : postData : HttpClientErrorException while calling rest client " + e.getResponseBodyAsString());
        }
        catch (HttpStatusCodeException e2) {
            RestClient.LOGGER.error("RestClient : postData : HttpStatusCodeException while calling rest client " + e2.getResponseBodyAsString());
        }
        catch (Exception e3) {
            RestClient.LOGGER.error("RestClient : postData : Exception while calling rest client ", (Throwable)e3);
        }
        return respsonseData;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger((Class)RestClient.class);
    }
}
