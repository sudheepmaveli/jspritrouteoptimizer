package com.jsprit.plotter.controller;

import com.jsprit.plotter.model.ArrangeLocation;
import com.jsprit.plotter.model.DeliveryLocation;
import com.jsprit.plotter.model.Response;
import com.jsprit.plotter.service.LocationArrange;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class LocationPlotter
{
  private static final Logger LOGGER = LoggerFactory.getLogger(LocationPlotter.class);
 
  @Autowired
  LocationArrange locationArrange;
  
  @GetMapping(value="/")
  public String index() {
	  LOGGER.info("###################");
	return "Greetings from Spring Boot!";
  }
  
  @PostMapping(value="/locations", produces="application/json")
  public Response arrangeLocations(@RequestBody ArrangeLocation location)
  {
    LOGGER.info("locationArrange");
    return this.locationArrange.arrangeLocation(location);
  }
}
