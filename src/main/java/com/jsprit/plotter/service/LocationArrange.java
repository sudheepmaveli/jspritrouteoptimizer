package com.jsprit.plotter.service;

import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.events.XMLEvent;

import org.apache.http.HttpHost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import com.graphhopper.jsprit.core.problem.cost.VehicleRoutingTransportCosts;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.VehicleRoute;
import com.graphhopper.jsprit.core.problem.solution.route.activity.TourActivity;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleType;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleTypeImpl;
import com.graphhopper.jsprit.core.util.Coordinate;
import com.graphhopper.jsprit.core.util.Solutions;
import com.graphhopper.jsprit.core.util.VehicleRoutingTransportCostsMatrix;
import com.graphhopper.jsprit.io.problem.VrpXMLReader;
import com.jsprit.plotter.model.ArrangeLocation;
import com.jsprit.plotter.model.DeliveryLocation;
import com.jsprit.plotter.model.DistanceMatrixRequest;
import com.jsprit.plotter.model.DistanceMatrixResponse;
import com.jsprit.plotter.model.Response;
import com.jsprit.plotter.model.Result;

@Component
public class LocationArrange {
	@Autowired
	RestTemplate restClient;
	private String proxyIp;
	private Integer proxyPort;
	private boolean isProxyNeeded = false;
	private String protocol;
	private static final Logger LOGGER = LoggerFactory.getLogger(LocationArrange.class);

	public <T> String getData(String url, T data) {
		LOGGER.info("RestClient : getData : url :" + url);
		String responseData = null;
		try {
			HttpComponentsClientHttpRequestFactory requestFactory = this.isProxyNeeded
					? new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create()
							.setProxy(new HttpHost(this.proxyIp, this.proxyPort.intValue(), this.protocol)).build())
					: new HttpComponentsClientHttpRequestFactory();

			URI targetUrl = new URIBuilder(url).build();
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			requestFactory.setReadTimeout(120000);
			requestFactory.setConnectTimeout(120000);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<T> request = new HttpEntity<T>(data, headers);

			responseData = (String) restTemplate.postForObject(targetUrl, request, String.class);
			LOGGER.info("RestClient : getData : respsonseData: Success");
			LOGGER.debug("RestClient : getData : respsonseData : " + responseData);
		} catch (HttpClientErrorException e) {
			LOGGER.error("RestClient : postData : HttpClientErrorException while calling rest client "
					+ e.getResponseBodyAsString());
		} catch (HttpStatusCodeException e) {
			LOGGER.error("RestClient : postData : HttpStatusCodeException while calling rest client "
					+ e.getResponseBodyAsString());
		} catch (Exception e) {
			LOGGER.error("RestClient : postData : Exception while calling rest client ", e);
		}
		return responseData;
	}

	public Response arrangeLocation(ArrangeLocation location) {
		
		List<DeliveryLocation> locations = location.getLocations();
		Map<String, DeliveryLocation> locationMap = new HashMap<>();
		for (int i = 0; i < locations.size(); i++) {
			locationMap.put("" + i, locations.get(i));
		}
		Location loc=Location.newInstance(0);

		VehicleType type = VehicleTypeImpl.Builder.newInstance("type").addCapacityDimension(0, 2).setCostPerDistance(1)
				.setCostPerTime(2).build();
		VehicleImpl vehicle = VehicleImpl.Builder.newInstance("vehicle").setStartLocation(loc)
				.setType(type).build();

		List<Service> services = serviceList(locations);

		DistanceMatrixResponse distanceMatrixResponse = getDistanceMatrix(locations);

		VehicleRoutingTransportCosts costMatrix = calculateCostMatrix(distanceMatrixResponse, locations.size());
		VehicleRoutingProblem.Builder vrp = VehicleRoutingProblem.Builder.newInstance();
		vrp.setFleetSize(FleetSize.INFINITE).setRoutingCost(costMatrix).addVehicle(vehicle);
		for (Service service : services) {
			vrp.addJob(service);
		}
		VehicleRoutingProblem vrps = vrp.build();
		/*
		 * solve the problem
		 */
		VehicleRoutingAlgorithm vra = Jsprit.Builder.newInstance(vrps).setProperty(Jsprit.Parameter.THREADS, "5")
				.buildAlgorithm();
		Collection<VehicleRoutingProblemSolution> solutions = vra.searchSolutions();
		System.out.println("solutions  :" + solutions);
		VehicleRoutingProblemSolution solution = Solutions.bestOf(solutions);
		System.out.println("solution  :" + solution);
		List<VehicleRoute> s = new ArrayList<>(solution.getRoutes());
		System.out.println("List<VehicleRoute>  :" + s);
		VehicleRoute route = s.get(0);
		List<TourActivity> ss = route.getActivities();
		LOGGER.info("TourActivity :" + ss);
		System.out.println("TourActivity :" + ss);
		List<String> order = new ArrayList<>();
		order.add("0");
		locationMap.remove("0");
		for (int i = 0; i < ss.size(); i++) {
			TourActivity tourActivity = ss.get(i);
			LOGGER.info("Order :" + tourActivity.getLocation().getId());
			order.add(tourActivity.getLocation().getId());
			locationMap.remove(tourActivity.getLocation().getId());
		}
		if (locationMap != null) {
			Set<String> keys = locationMap.keySet();
			for (String key : keys) {
				order.add(key);
			}
		}
		return getResponseData(order, locations);

	}

	public Response arrangeLocations(List<DeliveryLocation> locations) {

		int totalLocations = locations.size();
		DeliveryLocation startLocation = locations.get(0);
		DeliveryLocation endLocation = locations.get(totalLocations - 1);
		VehicleRoutingProblem.Builder vrpBuilder = VehicleRoutingProblem.Builder.newInstance();
		createXmlFile(locations);

		new VrpXMLReader(vrpBuilder).read("test.xml");

		int nuOfVehicles = 1;
		int capacity = 25;
		Coordinate firstDepotCoord = Coordinate.newInstance(startLocation.getLatitude(), startLocation.getLongitude());
		Coordinate endDepotCoord = Coordinate.newInstance(endLocation.getLatitude(), endLocation.getLongitude());
		int depotCounter = 1;
		for (Coordinate depotCoord : Arrays.asList(firstDepotCoord)) {
			for (int i = 0; i < nuOfVehicles; i++) {
				VehicleTypeImpl vehicleType = VehicleTypeImpl.Builder.newInstance(depotCounter + "_type")
						.addCapacityDimension(0, capacity).setCostPerDistance(1.0).build();
				VehicleImpl vehicle = VehicleImpl.Builder.newInstance(depotCounter + "_" + (i + 1) + "_vehicle")
						.setStartLocation(Location.newInstance(depotCoord.getX(), depotCoord.getY()))
						.setEndLocation(Location.newInstance(endDepotCoord.getX(), endDepotCoord.getY()))
						.setType(vehicleType).build();
				vrpBuilder.addVehicle(vehicle);
			}
			depotCounter++;
		}

		vrpBuilder.setFleetSize(FleetSize.FINITE);


		VehicleRoutingProblem vrp = vrpBuilder.build();

		/*
		 * solve the problem
		 */
		VehicleRoutingAlgorithm vra = Jsprit.Builder.newInstance(vrp).setProperty(Jsprit.Parameter.THREADS, "5")
				.buildAlgorithm();
		Collection<VehicleRoutingProblemSolution> solutions = vra.searchSolutions();
		VehicleRoutingProblemSolution solution = Solutions.bestOf(solutions);
		List<VehicleRoute> s = new ArrayList<>(solution.getRoutes());
		VehicleRoute route = s.get(0);
		List<TourActivity> ss = route.getActivities();
		LOGGER.info("TourActivity :" + ss);
		System.out.println("TourActivity :" + ss);
		List<String> order = new ArrayList<>();
		order.add("0");
		for (int i = 0; i < ss.size(); i++) {
			TourActivity tourActivity = ss.get(i);
			LOGGER.info("Order :" + tourActivity.getLocation().getId());
			order.add(tourActivity.getLocation().getId());
		}
		order.add("" + (locations.size() - 1));
		return getResponseData(order, locations);
	}

	private Response getResponseData(List<String> order, List<DeliveryLocation> locations) {
		Response response = new Response();
		response.setOrder(order);
		List<DeliveryLocation> deliveryOrder = new ArrayList<>();
		deliveryOrder.add(locations.get(0));
		for (String orderIndex : order) {
			deliveryOrder.add(locations.get(Integer.parseInt(orderIndex)));
		}
//		deliveryOrder.add(locations.get(locations.size() - 1));
		response.setData(deliveryOrder);
		serviceList(locations);
		System.out.println(response);
		return response;
	}

	private void createXmlFile(List<DeliveryLocation> locations) {
		try {
			FileOutputStream out = new FileOutputStream("test.xml");

			XMLOutputFactory output = XMLOutputFactory.newInstance();
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();

			// StAX Iterator API
			XMLEventWriter writer;

			writer = output.createXMLEventWriter(out);

			XMLEvent event = eventFactory.createStartDocument();
			// default
			event = eventFactory.createStartDocument("UTF-8", "1.0");
			writer.add(event);

			writer.add(eventFactory.createStartElement("", "", "problem"));
			writer.add(eventFactory.createAttribute("xmlns", "http://www.w3schools.com"));
			writer.add(eventFactory.createAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));
			writer.add(
					eventFactory.createAttribute("xsi:schemaLocation", "http://www.w3schools.com vrp_xml_schema.xsd"));

			writer.add(eventFactory.createStartElement("", "", "services"));

			for (int i = 1; i < (locations.size() - 1); i++) {
				writer.add(eventFactory.createStartElement("", "", "service"));

				writer.add(eventFactory.createAttribute("id", "" + i));
				writer.add(eventFactory.createAttribute("type", "service"));

				DeliveryLocation deliveryLocation = locations.get(i);

				writer.add(eventFactory.createStartElement("", "", "locationId"));
				writer.add(eventFactory.createCharacters("" + i));
				writer.add(eventFactory.createEndElement("", "", "locationId"));

				writer.add(eventFactory.createStartElement("", "", "coord"));
				writer.add(eventFactory.createAttribute("x", "" + deliveryLocation.getLatitude()));
				writer.add(eventFactory.createAttribute("y", "" + deliveryLocation.getLongitude()));
				writer.add(eventFactory.createEndElement("", "", "coord"));

				writer.add(eventFactory.createStartElement("", "", "capacity-demand"));
				writer.add(eventFactory.createCharacters("1"));
				writer.add(eventFactory.createEndElement("", "", "capacity-demand"));

				writer.add(eventFactory.createStartElement("", "", "duration"));
				writer.add(eventFactory.createCharacters("1"));
				writer.add(eventFactory.createEndElement("", "", "duration"));
				writer.add(eventFactory.createEndElement("", "", "service"));
			}
			writer.add(eventFactory.createEndElement("", "", "services"));
			writer.add(eventFactory.createEndElement("", "", "problem"));

			writer.add(eventFactory.createEndDocument());

			writer.flush();

			writer.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private DistanceMatrixResponse getDistanceMatrix(List<DeliveryLocation> deliveryLocations) {
		DistanceMatrixResponse distanceMatrixResponse = null;
		DistanceMatrixRequest distanceMatrixRequest = new DistanceMatrixRequest();
		distanceMatrixRequest.setOrigins(deliveryLocations);
		distanceMatrixRequest.setDestinations(deliveryLocations);
		distanceMatrixRequest.setTravelMode("driving");
		String jsonData = getData(
				"https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?"
						+ "key=AioSAaukbgWcjrpRe2eWLLEqfX_aBesqwPtcW5LoqWEhzLmcwnxCLKSC19JBBcSw",
				distanceMatrixRequest);
		LOGGER.info(jsonData);

		ObjectMapper mapper = new ObjectMapper();
		try {
			distanceMatrixResponse = mapper.readValue(jsonData, DistanceMatrixResponse.class);
			LOGGER.info(distanceMatrixResponse.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return distanceMatrixResponse;
	}

	private List<Service> serviceList(List<DeliveryLocation> locations) {

		List<Service> services = new ArrayList<>();
		for (int i = 1; i < locations.size(); i++) {
			Service service = Service.Builder.newInstance("" + i).addSizeDimension(0, 1)
					.setLocation(Location.newInstance("" + i)).build();
			services.add(service);
		}

		return services;
	}

	private VehicleRoutingTransportCosts calculateCostMatrix(DistanceMatrixResponse distanceMatrixResponse,
			int totalLocations) {

		List<Result> results = distanceMatrixResponse.getResourceSets().get(0).getResources().get(0).getResults();

		VehicleRoutingTransportCostsMatrix.Builder costMatrixBuilder = VehicleRoutingTransportCostsMatrix.Builder
				.newInstance(true);

//		for (int i = 0; i < (results.size() - 1); i++) {
		List<Result> filterResults = results.stream().filter(
				r -> r.originIndex != r.destinationIndex && r.destinationIndex > r.originIndex && r.travelDistance > 0)
				.collect(Collectors.toList());
		LOGGER.info("Filter Data:" + filterResults.toString());
		for (Result result : results) {
			LOGGER.info("Filter Data result:" + result.toString());
			LOGGER.info("getTravelDistance:" + result.getTravelDistance());
			LOGGER.info("getTravelDuration:" + result.getTravelDuration());
			if (result.getTravelDistance() > 0) {
				costMatrixBuilder.addTransportDistance("" + result.getOriginIndex(), "" + result.getDestinationIndex(),
						result.getTravelDistance());
				
				System.out.println("" + result.getOriginIndex()+ " : " + result.getDestinationIndex()+ " : "+result.getTravelDistance());
				
				costMatrixBuilder.addTransportTime("" + result.getOriginIndex(), "" + result.getDestinationIndex(),
						result.getTravelDuration());
				
				System.out.println("" + result.getOriginIndex()+ " : " + result.getDestinationIndex()+ " : "+result.getTravelDuration());
				System.out.println("---------------------------------------");
				
			}
		}

		LOGGER.info("costMatrixBuilder:" + costMatrixBuilder.toString());
//		}
		VehicleRoutingTransportCosts costMatrix = costMatrixBuilder.build();

		return costMatrix;

	}
	
	public static void main(String arg[]) {
		LocationArrange location= new LocationArrange();
		 
		List<DeliveryLocation> locations= new ArrayList<>();
		DeliveryLocation deliveryLocation0= new DeliveryLocation();
		//City max
		deliveryLocation0.setLatitude(25.25323047307187);
		deliveryLocation0.setLongitude(55.291591670491464);
		
		DeliveryLocation deliveryLocation1= new DeliveryLocation();
		//ADCB Metro
		deliveryLocation1.setLatitude(25.24479637171577);
		deliveryLocation1.setLongitude(55.29821152891279);
		
		DeliveryLocation deliveryLocation2= new DeliveryLocation();
		//Burjman Metro
		deliveryLocation2.setLatitude(25.25462236298354);
		deliveryLocation2.setLongitude(55.30441910839968);
		
		DeliveryLocation deliveryLocation3= new DeliveryLocation();
		//Hilton
		deliveryLocation3.setLatitude(25.251201660851045);
		deliveryLocation3.setLongitude(55.30073940040969);
		
		locations.add(deliveryLocation0);
		locations.add(deliveryLocation1);
		locations.add(deliveryLocation2);
		locations.add(deliveryLocation3);
		
		ArrangeLocation arrange=new ArrangeLocation();
		arrange.setLocations(locations);
		
		location.arrangeLocation(arrange);
		
		
	
	}
}
